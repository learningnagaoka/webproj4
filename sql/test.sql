drop database if exists testdb;

create database testdb;
use testdb;

create table users(
user_id int(11) primary key auto_increment,
user_name varchar(255),
password varchar(255)
);

INSERT INTO users VALUES(1, "田中", "tanaka");
INSERT INTO users VALUES(2, "佐藤", "satou");
INSERT INTO users VALUES(3, "鈴木", "suzuki");

create table inquiry(
id int(11) primary key auto_increment,
name varchar(255),
qtype varchar(255),
body varchar(255)
);