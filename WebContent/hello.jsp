<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="stylesheet" type="text/css" href="./css/style.css">
<title>Hello</title>
</head>
<body>

<table>
<tbody>

<tr>
<th>UserId</th>
<th>UserName</th>
<th>Password</th>
<th>Result</th>
</tr>

<s:iterator value="helloStrutsDTOList">
<tr>
<td><s:property value="userId"/></td>
<td><s:property value="username"/></td>
<td><s:property value="password"/></td>
<td><s:property value="result"/></td>
</tr>
</s:iterator>

</tbody>
</table>

</body>
</html>